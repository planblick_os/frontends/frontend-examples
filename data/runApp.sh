#!/bin/sh
_term() {
  echo "Caught SIGTERM signal!"
}

_termint() {
  echo "Caught SIGINT signal!"
}

trap _term SIGTERM
trap _termint SIGINT


curl --location --request POST 'http://kong.planblick.svc:8001/services/' \
--header 'Content-Type: application/json' \
--connect-timeout 5 \
--max-time 10 \
--retry 5 \
--retry-delay 0 \
--retry-max-time 40 \
--retry-connrefused \
--data-raw '{
  "name": "Simplycollect-frontend",
  "url": "http://simplycollect_frontend.planblick.svc:80"
}'

curl --location --request POST 'http://kong.planblick.svc:8001/services/Simplycollect-frontend/routes/' \
--header 'Content-Type: application/json' \
--connect-timeout 5 \
--max-time 10 \
--retry 5 \
--retry-delay 0 \
--retry-max-time 40 \
--retry-connrefused \
--data-raw '{
  "hosts": ["simplycollect.de", "www.simplycollect.de", "beta.simplycollect.de"]
}'

nginx