function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function register(form) {
    let login = $("#login").val()
    let pw = $("#password").val()
    let pwc = $("#password_confirmation").val()
    let privacy_agreement = $("#privacy_agreement")

    let pulse = 200
    if (login.length < 1 || pw.length < 1 || pwc.length < 1) {
        alert("Bitte füllen Sie alle Felder aus.")
        return false;
    }

    if (login.length < 5 || pw.length < 8) {
        $("#login").fadeOut(pulse).fadeIn(pulse).fadeOut(pulse).fadeIn(pulse)
        alert("Der Login muss mindestens 5, das Passwort mindestens 8 Zeichen lang sein.")
        return false;
    }

    if(!validateEmail(login)) {
        $("#login").fadeOut(pulse).fadeIn(pulse).fadeOut(pulse).fadeIn(pulse)
        alert("Bitte geben Sie eine valide Email-Addresse ein.")
        return false;
    }

    if (pw != pwc) {
        $("#password_confirmation").fadeOut(pulse).fadeIn(pulse).fadeOut(pulse).fadeIn(pulse)
        alert("Die Passwort-Wiederholung entspricht nicht dem Passwort")
        return false;
    }

    if (!privacy_agreement.is(':checked')) {
        privacy_agreement.fadeOut(pulse).fadeIn(pulse).fadeOut(pulse).fadeIn(pulse)
        alert("Sie müssen unsere Datenschutzerklärung akzeptieren um sich registrieren zu können.")
        return false;
    }


    $("#register_content").html("Einen Augenblick bitte...")

    var data = {"username": login, "password": pw}
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": API_BASE_URL + "/registration",
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
        },
        "processData": false,
        "data": JSON.stringify(data)
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        $("#register_content").html('Du erhältst in kürze eine Email von uns mit der Du die Registrierung abschließen kannst.<br/><br/>')
    });
}

$("#login").blur(function () {

    let login = $("#login").val()
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": API_BASE_URL + "/checklogin?login=" + login,
        "method": "GET",
        "headers": {
            "cache-control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {
        if(response.exists) {
            $("#login").css("border-color","red");
            $("#register_button").attr("disabled",true);
            $("#register_button").val("Login existiert bereits")
        } else {
            $("#login").css("border-color","green");
            $("#register_button").attr("disabled",false);
            $("#register_button").val("Registrieren")
        }

    });
});

