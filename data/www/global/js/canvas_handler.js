var wrapper = document.getElementById("action_wrapper");
var signature_pad_canvas_wrapper = document.getElementById("signature-pad-wrapper");

var canvas_width_multiplier = 0.8
var canvas_height_multiplier = 0.8
if (window.innerHeight < window.innerWidth) {
    canvas_width_multiplier = 0.8
    canvas_height_multiplier = 1.6
}

showWaitingForDocumentsTimer = ""
html_template = '<div class="signature-pads-container" style="position:relative"><div name="input_fields_container" id="input_fields_X" style="height:0px;"></div><canvas id="canvas_X" class="canvas canvas_X" name="Seite 1" data-url="Y"></canvas></div><br/>';
templates = new Array();
SignaturePads = new Array();
scale_ratio = 1


var clearButton = wrapper.querySelector("[data-action=clear]");
clearButton.addEventListener("click", function (event) {
    resetContainer();
    current_task = false;
});


function addTemplate(url) {
    templates.push(url);
}

function clearTemplates(url) {
    templates = []
}

function buildCanvasesFromTemplates(uris, inputfields) {

    signature_pad_canvas_wrapper.innerHTML = ""
    for (i = 0; i < uris.length; i++) {
        html_tmp = html_template.replace(/X/g, i)
        html_tmp = html_tmp.replace(/Y/g, uris[i])
        signature_pad_canvas_wrapper.innerHTML = signature_pad_canvas_wrapper.innerHTML + html_tmp
    }

    canvases = document.getElementsByTagName("canvas");

    for (i = 0; i < canvases.length; i++) {
        url = canvases[i].dataset.url;
        canvases[i].width = window.innerWidth * canvas_width_multiplier;
        canvases[i].height = window.innerHeight * canvas_height_multiplier;
        ;
        SignaturePads[i] = initPad(canvases[i])
        page = i
        if (typeof(inputfields) != "undefined" && typeof(inputfields[page + 1]) != "undefined") {
            loadBackgroundImage(canvases[i], url, inputfields[page + 1], page)
        }
        else {
            loadBackgroundImage(canvases[i], url, undefined, page)
        }
    }
    //activatePressure()
}

function initPad(canvas) {
    sp = new SignaturePad(canvas, {
        // It's Necessary to use an opaque color when saving image as JPEG;
        // this option can be omitted if only saving as PNG or SVG
        backgroundColor: 'rgb(255, 255, 255)',
        minDistance: 1,
        maxWidth: 1.1
    });
    sp.dotSize = 1;
    return sp;
}


function loadBackgroundImage(canvas, imagesource, inputfields, page) {
    imagesource = API_BASE_URL + "/easy2sign/gets3file/" + imagesource + "?apikey=" + getCookie("apikey")
    var background = new Image();
    background.setAttribute('crossOrigin', 'anonymous');
    some = new URLSearchParams(window.location)

    if (some.has('contract')) {
        alert(some.get('contract'));
        background.src = "assets/" + some.get('contract') + ".jpg";
    }
    else {
        background.src = imagesource;
    }
    background.setAttribute('crossOrigin', 'anonymous');

    // Make sure the image is loaded first otherwise nothing will draw.
    background.onload = function () {
        ctx = canvas.getContext("2d");
        ctx.scale(1, 1);
        ctx.imageSmoothingEnabled = true;

        src_width = background.width;
        src_height = background.height;
        background.width = window.innerWidth * canvas_width_multiplier;
        background.height = window.innerHeight * canvas_height_multiplier;
        scale_ratio = background.width / src_width;
        scale_ratio_y = background.height / src_height;
        //background.height = background.height * scale_ratio
        //console.debug(background.naturalWidth)
        console.log(scale_ratio)
        canvas.height = background.naturalHeight;
        canvas.width = background.naturalWidth;
        canvas.height = background.height;
        canvas.width = background.width;

        ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
        renderHtmlFields(inputfields, page)

    }
}

function renderHtmlFields(inputfields, page) {
    removeDocumentDataInputs(page)
    url = new URL(window.location)
    let urlsearch = new URLSearchParams(url.search)

    if (typeof(inputfields) != "undefined") {
        for (input_field in inputfields.reverse()) {
            input_data = inputfields[input_field]

            var input = document.createElement("input");
            if (typeof(input_data.height) == "undefined") {
                input_height = 40
            } else {
                input_height = input_data.height
            }
            input.ontouchstart = function (event) {
                console.log(event);
                event.preventDefault();
                this.focus();
                this.select();
                this.setSelectionRange(0, this.value.length)
            }
            input.parentNode = canvases[i]
            input.style.position = "absolute"
            input.style.top = input_data.pos_y * scale_ratio_y
            input.style.left = input_data.pos_x * scale_ratio
            input.size = input_data.size * scale_ratio
            input.style.height = input_height * scale_ratio_y
            input.style.display = "block"
            input.style.float = "absolute"
            var value = input_data.id

            if (urlsearch.has(input_data.id)) {
                value = urlsearch.get(input_data.id)
            }
            input.value = value

            input.id = input_data.id
            input.name = "document_data_field"
            input.dataset.page = page
            console.log("PAGE:" + page)
            $("#input_fields_" + page).prepend(input);

        }

    }
}

function removeDocumentDataInputs(page) {
    inputfields = document.querySelectorAll("[data-page='" + page + "']")
    for(field of inputfields) {
        field.parentNode.removeChild(field)
    }
}

function renderInputFields(renderCanvas, input_nodes) {
    ctx = renderCanvas.getContext("2d");
    ctx.font = '14px Verdana';

    for (input_idx = 0; input_idx < input_nodes.length; input_idx++) {
        if (input_nodes[input_idx].name == "document_data_field" && typeof(input_nodes[input_idx].style.top) != "undefined" && typeof(input_nodes[input_idx].style.left) != "undefined") {
            ctx.fillText(input_nodes[input_idx].value, parseInt(input_nodes[input_idx].style.left.replace("px", "")), (parseInt(input_nodes[input_idx].style.top.replace("px", "")) + 15))
            input_nodes[input_idx].style.visibility = "hidden"
        }
    }
}


var savePdfButton = wrapper.querySelector("[data-action=save-pdf]");
savePdfButton.addEventListener("click", function (event) {

    input_fields_containers = document.getElementsByClassName("input_fields_container")

    canvasses = document.getElementsByTagName("canvas");
    var data = new Array()
    data[0] = document_data
    for (i = 0; i < canvasses.length; i++) {
//console.log(canvasses[i].toDataURL());
        foobar = new Array()
        foobar["name"] = canvasses[i].getAttribute("name");
        foobar["dataUrl"] = canvasses[i].toDataURL();


        input_fields_nodes = canvasses[i].previousSibling.childNodes
        console.log(input_fields_nodes)
        input_fields_data = {}
        renderInputFields(canvasses[i], input_fields_nodes)

        data[i + 1] = canvasses[i].toDataURL();
        for (input_field_index in input_fields_nodes) {
            if (input_fields_nodes[input_field_index].name == "document_data_field") {
                if (typeof(input_fields_data[(i + 1)]) == "undefined") {
                    input_fields_data[(i + 1)] = {}
                }
                input_fields_data[(i + 1)][input_fields_nodes[input_field_index].id] = input_fields_nodes[input_field_index].value
            }
        }

        data[(i + 100)] = input_fields_data
    }

    $.blockUI({message: '<div class="responseMessage">Das Dokument wird verarbeitet. Einen Moment bitte ...</div>'});
    postUrl = API_BASE_URL + "/signature-pad-backend/upload";
    apikey = $.cookie("apikey");
    data["automatic_confirmation"] = true;
    console.log(data)
    $.ajax({
        type: "POST",
        url: postUrl,
        contentType: 'application/json;charset=UTF-8',
        data:
            JSON.stringify(data),
        beforeSend: function (xhr) {
            xhr.setRequestHeader('apikey', apikey);
        },
        success: function (response) {
            resetContainer();
            wrapper.style.display = "none";
            completeTask()
            namespace = "/" + encodeURI($.cookie("consumer_name").replace("@", "_"))
            console.log("USING SOCKETSERVER:" + SOCKET_SERVER + namespace)
            // Connect to the Socket.IO server.
            // The connection URL has the following format, relative to the current page:
            //     http[s]://<domain>:<port>[/<namespace>]

            var socket = io(SOCKET_SERVER + namespace);
            socket.on('connect', function () {
                socket.emit('my_broadcast_event', {data: 'Ein Dokument wurde gerade abgeschickt'});
            });
            socket.on('my_response', function (msg) {
                console.log("RECEIVED")
                console.log(msg)
            });
        },
        error: function () {
            $.blockUI({message: '<div class="responseMessage">Something went terribly wrong, sorry for that</div>'});
        }
    }).done(function (o) {
        console.log('saved');
    });

}, false);

function clearAll(url) {
    //url = document.getElementById("download_href").href;
    //window.open(url, '_blank');
    //window.focus();
    $.unblockUI();
}

function resetContainer() {
    templates = new Array();
    SignaturePads = new Array();
    signature_pad_canvas_wrapper.innerHTML = ""
}

waitingPopUpActive = false;

function setBlockWaitingForDocuments() {
    if (current_task == false && !waitingPopUpActive) {
        //$.blockUI({message: '<div class="responseMessage">Warte auf Dokumente...</div>'});
        $.blockUI({message: '<div onclick="clearAll(""); class="responseMessage">Geschafft!<br/> Bitte geben Sie das Ger&auml;t jetzt an der Rezeption ab.</div>'});
        waitingPopUpActive = true
    }
}

function activatePressure() {

    Pressure.set(document.getElementById("signature-pad-wrapper"), {
        change: function (force, event) {
            console.log("FORCE:", force);
            for (i = 0; i < SignaturePads.length; i++) {
                SignaturePads[i].dotSize = force * 1;
            }
        },
        unsupported: function () {
            for (i = 0; i < SignaturePads.length; i++) {
                SignaturePads[i].dotSize = 0.8;
            }
        }
    }, {polyfill: false});
}

