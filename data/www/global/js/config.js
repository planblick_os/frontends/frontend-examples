if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    API_BASE_URL = "http://localhost:8000"
    APP_BASE_URL = "http://localhost:7000"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "http://localhost:8000"
    COOKIE_DOMAIN = "crowdsoft.net"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
    USE_CUSTOM_CONSOLE_LOG = false
} else if (document.location.href.indexOf("http://fk.planblick.com") == 0) {
    console.log("Using DEV-Config")
    API_BASE_URL = "http://fk.planblick.com:8000"
    APP_BASE_URL = "http://fk.planblick.com:7000"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "http://fk.planblick.com:8000"
    COOKIE_DOMAIN = "crowdsoft.net"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
    USE_CUSTOM_CONSOLE_LOG = false
} else if (document.location.href.indexOf("https://backup.crowdsoft.net") == 0) {
    console.log("Using BACKUP-Config")
    API_BASE_URL = "https://backup.planblick.com"
    APP_BASE_URL = "https://backup.crowdsoft.net"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "https://backup.planblick.com"
    COOKIE_DOMAIN = "crowdsoft.net"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
    USE_CUSTOM_CONSOLE_LOG = true
}  else {
    console.log("Using LIVE-Config")
    API_BASE_URL = "https://api.planblick.com"
    APP_BASE_URL = "https://crowdsoft.net"
    APP_STARTPAGE = "/login/"
    SOCKET_SERVER = "https://api.planblick.com"
    COOKIE_DOMAIN = "crowdsoft.net"
    DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    SYSTEM_MESSAGE = ""
    USE_CUSTOM_CONSOLE_LOG = true
}

document.addEventListener("DOMContentLoaded", function () {
    if (SYSTEM_MESSAGE.length > 0) {
        let system_message_element = document.getElementById("system_message")
        if (system_message_element) {
            system_message_element.innerHTML = SYSTEM_MESSAGE
        }
    }
})

function copyToClipBoard(element_id) {
    var copyText = document.getElementById(element_id);
    copyText.select();
    document.execCommand("copy");
}

(function () {
    if (console.everything === undefined) {
        console.everything = [];

        console.defaultLog = console.log.bind(console);
        console.log = function () {
            console.everything.push({
                "location": window.location.href,
                "consumer": getCookie("consumer_name"),
                "username": getCookie("username"),
                "type": "log",
                "datetime": Date().toLocaleString(),
                "value": Array.from(arguments),
                "trace": console.trace
            });
            console.defaultLog.apply(console, arguments);
        }
        console.defaultError = console.error.bind(console);
        console.error = function () {
            console.everything.push({
                "location": window.location.href,
                "consumer": getCookie("consumer_name"),
                "username": getCookie("username"),
                "type": "error",
                "datetime": Date().toLocaleString(),
                "value": Array.from(arguments),
                "trace": console.trace
            });
            console.defaultError.apply(console, arguments);
        }
        console.defaultWarn = console.warn.bind(console);
        console.warn = function () {
            console.everything.push({
                "location": window.location.href,
                "consumer": getCookie("consumer_name"),
                "username": getCookie("username"),
                "type": "warn",
                "datetime": Date().toLocaleString(),
                "value": Array.from(arguments),
                "trace": console.trace
            });
            console.defaultWarn.apply(console, arguments);
        }
        console.defaultDebug = console.debug.bind(console);
        console.debug = function () {
            console.everything.push({
                "location": window.location.href,
                "consumer": getCookie("consumer_name"),
                "username": getCookie("username"),
                "type": "debug",
                "datetime": Date().toLocaleString(),
                "value": Array.from(arguments),
                "trace": console.trace
            });
            console.defaultDebug.apply(console, arguments);
        }
    }
})();

disableConsoleLog = !USE_CUSTOM_CONSOLE_LOG

function sendConsoleLog() {
    var seen = [];
    console.log("Consolelog-Connection-Check")
    if (!disableConsoleLog && typeof globalSocket != "undefined" && console.everything.length > 0) {
        try {
            globalSocket.emit('consolelog', JSON.stringify({data: console.everything}, function (key, val) {
                if (val != null && typeof val == "object") {
                    if (seen.indexOf(val) >= 0) {
                        return;
                    }
                    seen.push(val);
                }
                return val;
            }))
        } catch (err) {
            console.log("Cannot send console log to debug-server")
            console.log(err)
        }
    }
    console.everything = []
}

window.setInterval(sendConsoleLog, 10000)

news_config = [
    /*{
        news_id: "2020_04_16",
        show_on_date: "2020_04_16",
        content_uri: "../news/index.html",
        content_id: "2020_04_16",
        show_in_apps: ["easy2schedule"],
        force_display: true
    },*/
]

get_news_for = function (app) {
    for (let news of news_config) {
        if (news.show_in_apps.includes(app)) {
            $.ajax({
                type: 'GET',
                url: news.content_uri,
                dataType: 'html',
                success: function (data) {
                    let evt = new Event('news_available');
                    evt.content_id = news.content_id
                    evt.force_display = news.force_display
                    evt.data = data
                    document.dispatchEvent(evt);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("ERROR NEWS", errorThrown)
                }
            });

        }
    }
}
