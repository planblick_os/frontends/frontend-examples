document.addEventListener("DOMContentLoaded", function (event) {
    get_translations()
    document.addEventListener('readyforcalendarload', runCalendar)
    getById('appointment').addEventListener("submit", fnAppointmentsubmit, false);
    document.addEventListener("keydown", handleKeyPress, false);
    window.addEventListener("touchstart", touchstart, false);
    window.addEventListener("touchend", touchend, false);
    getById('filter_text').addEventListener("keyup", filter_events, false);

    initCalendar();
    document.addEventListener("news_available", renderNews, false);
    get_news_for("easy2schedule")
    $('.menu .item')
        .tab()
    ;
});

window.addEventListener("viewSkeletonRender_finished", function () {
    console.log("SKELETONrender listener")
    urlParams = new URLSearchParams(window.location.search)

    if (urlParams.has('viewdate')) {
        setViewDate(urlParams.get('viewdate'));
    }

    if (urlParams.has('newAppointment')) {
        let startdate = null
        if (urlParams.has('startdate')) {
            startdate = urlParams.get('startdate')
        }
        let enddate = null
        if (urlParams.has('enddate')) {
            enddate = urlParams.get('enddate')
        }
        if(startdate && !enddate) {
            enddate = moment(startdate, datetimeFormat).add(60, "minutes").format(datetimeFormat)
        }

        let title = ""
        if (urlParams.has('title')) {
            title = urlParams.get('title')
        }

        setFormFields('appointment', null, startdate, enddate, title)
    }

}, false);
