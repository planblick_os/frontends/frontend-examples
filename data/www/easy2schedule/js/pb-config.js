import {BASECONFIG} from "./pbjs/pb-config.js";
export const CONFIG = BASECONFIG

CONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "./js/pbjs/vendor/jquery/jquery.cookie.js", loaded: null},
    {name: "jquery.blockUI", path: "./js/pbjs/vendor/jquery/jquery.blockUI.js", loaded: null},
    {name: "socket.io", path: "./js/pbjs/vendor/socket.io.js", loaded: null},
]

if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    CONFIG.CIRCUIT_BASE_URL = "http://localhost:7000/easy2schedule/circuits/"
    CONFIG.APP_BASE_URL = "http://localhost:7000"
    CONFIG.API_BASE_URL = "http://localhost:8000"
    CONFIG.SOCKET_SERVER = "http://localhost:8000"

} else if (document.location.href.indexOf("http://fk.planblick.com") == 0) {
    console.log("Using DEV-Config")
} else {
    console.log("Using LIVE-Config")
    CONFIG.APP_BASE_URL = "https://crowdsoft.net/easy2schedule/"
    CONFIG.LOGIN_URL = "https://crowdsoft.net/beta/login/"
}