export class pbBatch {
    constructor() {
        this.stack = []
        this.processed = []
        this.interval = undefined

        if (!pbBatch.instance) {
            pbBatch.instance = this;
        }
        pbBatch.instance.startBatchProcessing()
        return pbBatch.instance;
    }

    startBatchProcessing() {
        pbBatch.instance.interval = setInterval(pbBatch.instance.processBatch, 200)
    }

    stopBatchProcessing() {
        clearInterval(pbBatch.instance.interval)
    }

    processBatch() {
        if(pbBatch.instance.stack[0] && pbBatch.instance.stack[0]["state"] == -1) {
            console.log("Stack Entry ID", pbBatch.instance.stack[0]["id"], "will be started now")
            pbBatch.instance.stack[0]["function"]()
            pbBatch.instance.stack[0]["state"] = 0
            if(pbBatch.instance.stack[0]["auto_resolve"] === true) {
                pbBatch.instance.resolve(pbBatch.instance.stack[0]["id"])
            }
        }
    }

    restart(id) {
        for(let i in pbBatch.instance.stack) {
            if(pbBatch.instance.stack[i]["id"] == id) {
                pbBatch.instance.stack[i]["state"] = -1
            }
        }
    }

    resolve(id) {
        for(let i in pbBatch.instance.stack) {
            if(pbBatch.instance.stack[i]["id"] == id) {
                pbBatch.instance.stack[i]["state"] = 1
                pbBatch.instance.processed.push(pbBatch.instance.stack[i])
                pbBatch.instance.delete(pbBatch.instance.stack[i]["id"])
            }
        }
        console.log("Processed stack:", pbBatch.instance.processed)
    }

    add(id, func, auto_resolve=false) {
        if(id && func){
            let stack_entry = {"id": id, "state": -1, "function": func, "auto_resolve": auto_resolve}
            pbBatch.instance.stack.push(stack_entry)
        }

    }

    delete(id) {
        for(let i in pbBatch.instance.stack) {
            if(id == pbBatch.instance.stack[i]["id"]) {
                pbBatch.instance.stack.splice(i,1)
            }
        }
    }
}