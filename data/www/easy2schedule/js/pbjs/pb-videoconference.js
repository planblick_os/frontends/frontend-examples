import {CONFIG} from "../pb-config.js";
import {fireEvent} from "./pb-functions.js"

//https://jitsi.github.io/handbook/docs/dev-guide/dev-guide-iframe

export class PbVideoconference {
    constructor(roomName, displayNode = '#meet', displayName = "Gast", width = "100%", height = "100%",) {
        let customer_session = roomName

        $("#meet").html("")
        if (PbVideoconference.instance) {
            PbVideoconference.instance.setOptions(displayName, roomName, displayNode, height, width)
            return PbVideoconference.instance;
        }

        PbVideoconference.instance = this
        PbVideoconference.instance.initalized = false;
        PbVideoconference.instance.domain = CONFIG.JITSI_DOMAIN
        PbVideoconference.instance.setOptions(displayName, roomName, displayNode, height, width)
        PbVideoconference.instance.api = null
        PbVideoconference.instance.current_roomname = null
        return PbVideoconference.instance
    }

    setOptions(
        displayName,
        roomName,
        displayNode,
        height = null,
        width = null,
        toolbar_buttons = ['microphone', 'camera', 'desktop', 'fullscreen',
            'fodeviceselection', 'settings',
            'videoquality', 'filmstrip', 'stats', 'shortcuts',
            'tileview']
        /* microphone', 'camera', 'closedcaptions', 'desktop', 'embedmeeting', 'fullscreen',
        'fodeviceselection', 'hangup', 'profile', 'chat', 'recording',
        'livestreaming', 'etherpad', 'sharedvideo', 'settings', 'raisehand',
        'videoquality', 'filmstrip', 'invite', 'feedback', 'stats', 'shortcuts',
        'tileview', 'videobackgroundblur', 'download', 'help', 'mute-everyone', 'security' */
    ) {
        PbVideoconference.instance.options = {
            // https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js
            interfaceConfigOverwrite: {
                APP_NAME: 'SimplyCollect',
                NATIVE_APP_NAME: 'SimplyCollect',
                PROVIDER_NAME: 'SimplyCollect',
                RECENT_LIST_ENABLED: false,
                TOOLBAR_ALWAYS_VISIBLE: false,
                TOOLBAR_BUTTONS: toolbar_buttons,
                DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
                MOBILE_APP_PROMO: true,
                VIDEO_LAYOUT_FIT: 'both',
                ENFORCE_NOTIFICATION_AUTO_DISMISS_TIMEOUT: 15,
                TILE_VIEW_MAX_COLUMNS: 2,
                DISABLE_FOCUS_INDICATOR: true
            },
            configOverwrite: {
                disableDeepLinking: true
            },
            userInfo: {
                email: '',
                displayName: displayName
            },
            roomName: roomName,
            width: width,
            height: height,
            parentNode: document.querySelector(displayNode)
        }
        return PbVideoconference.instance
    }

    init() {

        if (PbVideoconference.instance.api != null) {
            PbVideoconference.instance.api.dispose();
            fireEvent('conference_disposed', {"roomName": PbVideoconference.instance.current_roomname})
        }
        PbVideoconference.instance.api = new JitsiMeetExternalAPI(PbVideoconference.instance.domain, PbVideoconference.instance.options)
        PbVideoconference.instance.current_roomname = PbVideoconference.instance.options.roomName
        PbVideoconference.instance.initalized = true
        fireEvent('conference_initialized', {"roomName": PbVideoconference.instance.options.roomName})

        return PbVideoconference.instance

    }

    hangup() {
        PbVideoconference.instance.api.addListener("videoConferenceLeft", function() {$("#meet").html("")});
        PbVideoconference.instance.api.executeCommand("hangup")
    }

    mute() {
        PbVideoconference.instance.muteAudio()
        PbVideoconference.instance.muteVideo()
    }

    setLobbyMode(enabled = true) {
        PbVideoconference.instance.api.executeCommand('toggleLobby', enabled);
    }

    setDisplayName(name) {
        PbVideoconference.instance.api.executeCommand('displayName', name);
    }

    muteAudio() {
        PbVideoconference.instance.api.isAudioMuted().then(muted => {

        });

    }

    muteVideo() {
        PbVideoconference.instance.api.isVideoMuted().then(muted => {
            alert(muted)
        });
    }
}