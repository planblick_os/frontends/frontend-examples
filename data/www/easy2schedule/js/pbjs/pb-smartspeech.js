import {getCookie, md5, localspeech} from "./pb-functions.js";
import {socketio} from "./pb-socketio.js";

export class pbSmartspeech {
    constructor() {
        if (!pbSmartspeech.instance) {
            pbSmartspeech.instance = this;
        }
        return pbSmartspeech.instance;
    }

    say(text = "foobar", callback = ()=>"") {
        document.addEventListener("command", function _(event) {
            console.log("COMMAND", event)
            if (event.data.command == "smartSpeech.say") {
                //alert("Wav File wurde auf dem Server gefunden und wird an den Browser gestreamt.")
                console.log("SMARTSPEECH", event.data)
                pbSmartspeech.instance.playByteArray(event.data.args.data, callback)
            }
            if (event.data.command == "localspeech") {
                //alert("Verwende lokale Browser-TextToSpeech-Engine.")
                console.log("LOCALSPEECH", event.data)
                localspeech(event.data.args.data, callback)
            }
            document.removeEventListener("command", _)
        })
        new socketio().socket.emit("command", {
            command: "smartSpeech.say",
            args: {"text": text, "say_to_room": getCookie("username")},
            room: "tmp_worker"
        })
    }

    playByteArray(arrayBuffer, callback) {
        var context = new AudioContext();
        context.decodeAudioData(arrayBuffer.slice(0), function (buffer) {
            //pbSmartspeech.instance.play(buffer);

            // Create a source node from the buffer
            var source = context.createBufferSource();
            source.buffer = buffer;
            // Connect to the final output node (the speakers)
            source.connect(context.destination);
            // Play immediately
            source.start(0);
            source.addEventListener("ended", function (event) {
                context.close()
                context = undefined
                arrayBuffer = undefined
                callback()
            })

        });
    }
}

