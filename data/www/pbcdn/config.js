export const CONFIG = {}
if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    CONFIG.API_BASE_URL = "http://localhost:8000"
    CONFIG.APP_BASE_URL = "http://localhost:7000"
    CONFIG.APP_STARTPAGE = "/login/"
    CONFIG.SOCKET_SERVER = "http://localhost:8000"
    CONFIG.COOKIE_DOMAIN = "crowdsoft.net"
    CONFIG.DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.SYSTEM_MESSAGE = ""
    CONFIG.JS_CACHE_ACTIVE = false
    CONFIG.SEND_CONSOLE_LOG = true
    CONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 5000
    CONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
    CONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
} else if (document.location.href.indexOf("http://fk.planblick.com") == 0) {
    console.log("Using DEV-Config")
    CONFIG.API_BASE_URL = "http://fk.planblick.com:8000"
    CONFIG.APP_BASE_URL = "http://fk.planblick.com:7000"
    CONFIG.APP_STARTPAGE = "/login/"
    CONFIG.SOCKET_SERVER = "http://fk.planblick.com:8000"
    CONFIG.COOKIE_DOMAIN = "crowdsoft.net"
    CONFIG.DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.SYSTEM_MESSAGE = ""
    CONFIG.JS_CACHE_ACTIVE = false
    CONFIG.SEND_CONSOLE_LOG = true
    CONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 3000
    CONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
    CONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
} else {
    console.log("Using LIVE-Config")
    CONFIG.API_BASE_URL = "https://api.planblick.com"
    CONFIG.APP_BASE_URL = "https://crowdsoft.net"
    CONFIG.APP_STARTPAGE = "/login/"
    CONFIG.SOCKET_SERVER = "https://api.planblick.com"
    CONFIG.COOKIE_DOMAIN = "crowdsoft.net"
    CONFIG.DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.SYSTEM_MESSAGE = ""
    CONFIG.JS_CACHE_ACTIVE = false
    CONFIG.SEND_CONSOLE_LOG = true
    CONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 3000
    CONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
    CONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
}
