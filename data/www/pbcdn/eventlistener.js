import {Init} from './init.js'
import {pb_events} from './events.js'
import {i18n} from './i18n.js'
import {CONFIG} from './config.js';
import {STATE} from './globalState.js';
import {Account} from './account-api.js';

export function add_pb_listeners() {
    document.addEventListener("DOMContentLoaded", function (event) {
        new pb_events().content_switched("content_1")
    })

    document.addEventListener("content_switched", function (el) {
        let content_id = el.detail.content_id;
        $('[data-content]').removeClass("active")
        $('[data-content="' + content_id + '"]').addClass("active")
    }, false);

    document.addEventListener("js_sources_loaded", function () {
        new Init().init_submodules()
    }, false);

    document.addEventListener("jquery_cookie_loaded", function () {
        new i18n().get_translations()
    }, false);

    document.addEventListener("js_sources_loaded", function () {
        new Account().checkLogin()
    }, false);

    document.addEventListener("i18n_ready", function () {
        new i18n().replace_i18n()
        $("#spinner").css("display", "none")
    }, false);

    document.addEventListener("js_file_load_failed", function _js_file_load_failed(event) {
        if (!STATE["file_load_tries"][event.data.name]) {
            STATE["file_load_tries"][event.data.name] = 0
        }

        STATE["file_load_tries"][event.data.name] += 1

        if (STATE["file_load_tries"][event.data.name] < CONFIG.FILE_LOAD_FAILED_MAX_RETRIES) {
            setTimeout(function () {
                Init.instance.load_file(event.data)
            }, CONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT)
        } else {
            console.error("File" + event.data.path + " cannot be loaded after " + STATE["file_load_tries"] + " Giving up now")
        }

    }, false);

}

