export class pb_events {
    constructor() {}
    get_event(eventname, options, fire = true) {
        if (this[eventname]) {
            return this[eventname](options)
        }
    }
    fire(event) {
        document.dispatchEvent(event);
    }
    content_switched(element, fire= true) {
        let content_id = null
        if(typeof element == 'string') {
            content_id = element
        } else {
            content_id = $(element).attr("data-content")
        }

        let event = new CustomEvent("content_switched", {detail: {content_id: content_id}})
        if (fire) {
            this.fire(event)
        }
        return event
    }
}
