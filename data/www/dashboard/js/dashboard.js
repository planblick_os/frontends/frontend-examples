checkLogin()

function getCalenderExportUri() {
    let currentYear = new Date().getFullYear()
    let apikey = $.cookie("apikey")
    let calenderExportUri = API_BASE_URL + "/cb/events?apikey=" + apikey + "&start=" + currentYear + "-01-01T00%3A00%3A00&end=" + currentYear + "-12-31T23%3A59%3A59&timeZone=Europe%2FBerlin&format=json"
    return calenderExportUri
}
var noLicenseText="<hr/>Sie haben aktuell keine Lizenz für diese Anwendung."
var availableApps = [
    {
        name: "Quicksign",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/access/quicksign",
        hide_if_not_available: true,
        tooltip: {text: "Digitale Vertragszeichnung einfach und für Jedermann.", position: "right"},
        href: "/quicksign/"
    },
    {
        name: "Easy2Sign Admin",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/access/easy2sign",
        hide_if_not_available: true,
        tooltip: {text: "Digitale Vertragszeichnung und -verwaltung einfach und ohne elektronische Signaturen.", position: "right"},
        href: "/easy2sign/admin.html"
    },
    {
        name: "Easy2Sign Client",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/access/easy2sign",
        hide_if_not_available: true,
        tooltip: {text: "Digitale Vertragszeichnung und -verwaltung einfach und ohne elektronische Signaturen.", position: "right"},
        href: "/easy2sign/beta.html"
    },
    {
        name: "Easy2Schedule",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/access/easy2schedule",
        hide_if_not_available: false,
        tooltip: {text: "Kalenderbasierte Planung von Ressourcen und Personen.", position: "right"},
        href: "/easy2schedule/"
    },
    {
        name: "Easy2Schedule Daten-Export",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/access/easy2schedule",
        hide_if_not_available: true,
        tooltip: {text: "Export der Kalenderdaten des laufenden Jahres", position: "right"},
        href: getCalenderExportUri()
    },
    {
        name: "Easy2Track Daten-Export",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/easy2track/healthz",
        hide_if_not_available: true,
        tooltip: {text: "Export der Kontaktverfolgung im Falle einer Covid19 Erkrankung", position: "right"},
        href: "/easy2track/request_export.html"
    },
    {
        name: "Easy2Track Besucher",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/easy2track/healthz",
        hide_if_not_available: true,
        tooltip: {text: "Liste der Besucher mit Verweildauer", position: "right"},
        href: "/easy2track/visitors.html"
    },
    {
        name: "Easy2Checkin (ALPHA)",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/access/easy2checkin",
        hide_if_not_available: true,
        tooltip: {text: "Selbst-Anmeldung von Besuchern oder Patienten durch eine sprachbasierte, digitale Rezeption.", position: "right"},
        href: "/reception/"
    },
    {
        name: "planBLICK Verwaltung",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/access/superadmin",
        hide_if_not_available: true,
        tooltip: {text: "Admin-Panel for planBLICK only", position: "right"},
        href: "/superadmin/index.html"
    },
    {
        name: "Easy2Monitor (In Development)",
        icon_url: "/global/img/appicons/logo_quadrat.png",
        availability_check: API_BASE_URL + "/access/superadmin",
        hide_if_not_available: true,
        tooltip: {text: "Status-Übersicht der Crowdsoft-Services", position: "right"},
        href: "/easy2monitor/index.html"
    }
]

function renderAppLinks() {
    document.getElementById("pbapps").innerHTML=""
    for (let app of availableApps) {
        let app_item_wrapper = document.createElement("a");
        let app_item_icon = document.createElement("img");
        let app_item_text = document.createElement("span");

        app_item_wrapper.id = app.name
        app_item_wrapper.classList.add("app_container")
        app_item_wrapper.href = app.href
        app_item_wrapper.setAttribute("data-html","true")
        app_item_wrapper.classList.add("inactive")
        app_item_wrapper.style.display="none"

        if(app.tooltip != null) {
            app_item_wrapper.setAttribute("data-toggle","tooltip")
            app_item_wrapper.setAttribute("data-placement", app.tooltip.position)
            app_item_wrapper.title = app.tooltip.text
        }

        app_item_icon.src = app.icon_url
        app_item_icon.classList.add("app_icon")
        app_item_text = app.name

        app_item_wrapper.append(app_item_icon)
        app_item_wrapper.innerHTML += "<br/>"
        app_item_wrapper.append(app_item_text)
        document.getElementById("pbapps").append(app_item_wrapper)

        setAppStatus(app, app_item_wrapper)
    }
}

function setAppStatus(app, wrapper_element) {
   var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            console.log(this)

            if (this.status == 200 || this.status == 204) {
                wrapper_element.classList.remove("inactive")
                wrapper_element.classList.add("active")
                wrapper_element.style.display="inline-grid"

            } else {
                wrapper_element.classList.remove("active")
                wrapper_element.classList.add("inactive")
                wrapper_element.setAttribute("data-toggle","tooltip")
                wrapper_element.setAttribute("data-placement", app.tooltip && app.tooltip.position ? app.tooltip.position:"right")
                wrapper_element.title = wrapper_element.title + noLicenseText
                wrapper_element.href ="#"
                if(app.hide_if_not_available) {
                    wrapper_element.style.display="none"
                    wrapper_element.parentNode.removeChild(wrapper_element)
                }
                else {
                    wrapper_element.style.display="inline-grid"
                }

            }
            $(wrapper_element).tooltip('fixTitle')

        }
    });

    xhr.open("GET", app.availability_check);
    xhr.setRequestHeader("apikey", getCookie("apikey"));
    xhr.send();
}

function renderProfileData() {
    return;
    let explanation_text = document.createElement("div");
    explanation_text.innerHTML = "Diese Informationen helfen uns beim finden von Problemen.<br/>Bitte teilen Sie uns diese bei Support-Anfragen immer mit.<br/>"
    document.getElementById("profile_wrapper").append(explanation_text)

    let debugContent = document.createElement("textarea");
    debugContent.value = "Consumer: " + decodeURIComponent(getCookie("consumer_name")) + "\nUsername: " + decodeURIComponent(getCookie("username")) + "\nAPI-Key: " + getCookie("apikey")
    debugContent.id="debugContent"
    debugContent.style.width="50%"
    debugContent.style.height="7rem"

    let copybutton = document.createElement("button");
    copybutton.onclick = copyToClipBoard.bind(this, "debugContent")
    copybutton.innerHTML = "Daten Kopieren"
    copybutton.style.display = "block"

    document.getElementById("profile_wrapper").append(debugContent)
    document.getElementById("profile_wrapper").append(copybutton)


}

document.addEventListener("DOMContentLoaded", function (event) {
    renderAppLinks()
    renderProfileData()
});
